import styleSchema from "./styleSchema.js";

let style = styleSchema.map((x) => {
  return {
    ...x,
    configs: x.configs.map((s) => {
      if (s.input === "slider") {
        // console.log(s.label, s.max);
        if (s.max === undefined || s.max === NaN || s.max === false) {
          s.max = 100;
        } else {
          s.max = parseInt(s.max);
        }

        s.min = 0;
      }
      if (s.input === undefined) {
        console.log(s.label, "UNDEFINED");
      }
      return s;
    }),
    mobile: x.mobile.map((s) => {
      if (s.input === "slider") {
        // console.log(s.label, s.max);
        if (s.max === undefined || s.max === NaN || s.max === false) {
          s.max = 100;
        } else {
          s.max = parseInt(s.max);
        }

        s.min = 0;
      }
      if (s.input === undefined) {
        console.log(s.label, "UNDEFINED");
      }
      return s;
    }),
    desktop: x.desktop.map((s) => {
      if (s.input === "slider") {
        // console.log(s.label, s.max);
        if (s.max === undefined || s.max === NaN || s.max === false) {
          s.max = 100;
        } else {
          s.max = parseInt(s.max);
        }

        s.min = 0;
      }
      if (s.input === undefined) {
        console.log(s.label, "UNDEFINED");
      }
      return s;
    }),
  };
});

const preset = JSON.stringify(style);

export default style;
