export const maxWidth = {
  label: "Max Width",
  property: "max-width",
  value: "1000px",
  input: "slider",
  unit: "px",
  max: "1280",
  type: "style",
};

export const width = {
  label: "Width",
  property: "width",
  value: "100%",
  input: "none",
  type: "style",
};

export const borderRadius = {
  label: "Border Radius",
  property: "border-radius",
  value: "8px",
  input: "slider",
  unit: "px",
  type: "style",
};

export const borderColor = {
  label: "Border Color",
  property: "border-color",
  value: "rgba(0,0,0,1)",
  input: "color",
  type: "style",
};

export const borderWidth = {
  label: "Border Width",
  property: "border-width",
  value: "1px",
  input: "slider",
  unit: "px",
  max: "10",
  type: "style",
};

export const column = {
  label: "Column",
  property: "column",
  value: "w-6/12",
  input: "select",
  options: [
    { value: "w-full", text: "One" },
    { value: "w-6/12", text: "Two" },
    { value: "w-4/12", text: "Three" },
    { value: "w-3/12", text: "Four" },
  ],
  type: "class",
};

export const backgroundColorHover = {
  label: "Hover Background Color",
  property: "--background-color-hover",
  value: "rgba(113, 99, 99,1)",
  input: "color",
  type: "style",
};

export const backgroundColor = {
  label: "Background Color",
  property: "background-color",
  value: "rgba(255,255,255,1)",
  input: "color",
  type: "style",
};

export const backgroundGradient = {
  label: "Background Color",
  property: "background",
  value: "rgba(255,255,255,1)",
  meta: {
    type: "color",
    gradient: {
      type: "linear",
      degree: 0,
      points: [
        {
          alpha: 1,
          red: 4,
          blue: 255,
          green: 0,
          left: 0,
        },
        {
          alpha: 1,
          red: 0,
          blue: 212,
          green: 255,
          left: 100,
        },
      ],
      style:
        "linear-gradient(0deg,rgba(4, 0, 255, 1) 0%,rgba(0, 255, 212, 1) 100%",
    },
    color: {
      alpha: 1,
      blue: 255,
      green: 255,
      red: 255,
      saturation: 0,
      style: "rgba(255, 255, 255, 1)",
      value: 1000,
    },
    image: {
      url:
        "https://images.unsplash.com/photo-1593642634315-48f5414c3ad9?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80",
      style:
        "url('https://images.unsplash.com/photo-1593642634315-48f5414c3ad9?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80')",
    },
  },
  input: "gradient",
  type: "style",
};

export const backgroundGradientHover = {
  label: "Background Color Hover",
  property: "--background-hover",
  value: "rgba(255,255,255,1)",
  meta: {
    type: "color",
    gradient: {
      type: "linear",
      degree: 0,
      points: [
        {
          alpha: 1,
          red: 4,
          blue: 255,
          green: 0,
          left: 0,
        },
        {
          alpha: 1,
          red: 0,
          blue: 212,
          green: 255,
          left: 100,
        },
      ],
      style:
        "linear-gradient(0deg,rgba(4, 0, 255, 1) 0%,rgba(0, 255, 212, 1) 100%",
    },
    color: {
      alpha: 1,
      blue: 255,
      green: 255,
      red: 255,
      saturation: 0,
      style: "rgba(255, 255, 255, 1)",
      value: 1000,
    },
  },
  input: "gradient",
  type: "style",
};

export const cursor = {
  label: "Cursor",
  property: "cursor",
  value: "pointer",
  input: "none",
  type: "style",
};

export const backgroundImage = {
  label: "Background Image",
  property: "background-image",
  value:
    "url('https://images.unsplash.com/photo-1593642634315-48f5414c3ad9?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80')",
  input: "image",
  type: "style",
};

export const backgroundRepeat = {
  label: "Background Repeat",
  property: "background-repeat",
  value: "no-repeat",
  input: "none",
  type: "style",
};

export const backgroundAttachment = {
  label: "Background Attachment",
  property: "background-attachment",
  value: "fixed",
  input: "none",
  type: "style",
};

export const backgroundPosition = {
  label: "Background Position",
  property: "background-position",
  value: "center center",
  input: "none",
  type: "style",
};

export const backgroundSize = {
  label: "Background Size",
  property: "background-size",
  value: "cover",
  input: "none",
  type: "style",
};

export const fontFamily = {
  label: "Font",
  property: "font-family",
  value: "Open Sans",
  meta: {
    url: "",
    font: "",
  },
  input: "font",
  type: "style",
};

export const show = {
  label: "Show",
  property: "display",
  value: "flex",
  input: "select",
  options: [{ value: "flex", text: "Show" }, { value: "none", text: "Hide" }],
  type: "style",
};

export const showBlock = {
  label: "Show",
  property: "display",
  value: "block",
  input: "select",
  options: [{ value: "block", text: "Show" }, { value: "none", text: "Hide" }],
  type: "style",
};
