export const textAlign = {
  label: "Text Align",
  property: "text-align",
  value: "left",
  input: "select",
  options: [
    { value: "center", text: "Center" },
    { value: "left", text: "Left" },
    { value: "right", text: "Right" },
    { value: "justify", text: "Justify" },
  ],
  type: "style",
};
export const fontSize = {
  label: "Font Size",
  property: "font-size",
  value: "17px",
  input: "slider",
  unit: "px",
  type: "style",
};
export const fontWeight = {
  label: "Font Weight",
  property: "font-weight",
  value: "400",
  input: "select",
  options: [
    { value: "300", text: "Thin" },
    { value: "400", text: "Regular" },
    { value: "600", text: "Bold" },
    { value: "800", text: "Extra Bold" },
  ],
  type: "style",
};
export const color = {
  label: "Color",
  property: "color",
  value: "rgba(0,0,0,1)",
  input: "color",
  type: "style",
};
