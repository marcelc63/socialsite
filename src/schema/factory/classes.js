export const flex = {
  label: "Flex",
  property: "flex",
  value: "flex",
  input: "none",
  type: "class",
};

export const overflow = {
  label: "Overflow",
  property: "overflow",
  value: "overflow-y-scroll",
  input: "none",
  type: "class",
};

export const height = {
  label: "Height",
  property: "mobile-height",
  value: "min-h-full",
  input: "none",
  type: "class",
};

export const flexDirection = {
  label: "Flex Direction",
  property: "flex-direction",
  value: "flex-col",
  input: "select",
  options: [
    { value: "flex-col", text: "Column" },
    { value: "flex-row", text: "Row" },
  ],
  type: "class",
};

export const flexRow = {
  label: "Flex Direction",
  property: "flex-direction",
  value: "flex-row",
  input: "none",
  type: "class",
};

export const flexCol = {
  label: "Flex Direction",
  property: "flex-direction",
  value: "flex-col",
  input: "none",
  type: "class",
};

export const flexWrap = {
  label: "Flex Wrap",
  property: "flex-wrap",
  value: "flex-wrap",
  input: "none",
  type: "class",
};

export const alignment = {
  label: "Alignment",
  property: "alignment",
  value: "items-center",
  input: "select",
  options: [
    { value: "items-center", text: "Center" },
    { value: "items-start", text: "Top" },
  ],
  type: "class",
};

export const itemsCenter = {
  label: "Alignment",
  property: "alignment",
  value: "items-center",
  input: "none",
  type: "class",
};

export const itemsStart = {
  label: "Alignment",
  property: "alignment",
  value: "items-start",
  input: "none",
  type: "class",
};

export const justify = {
  label: "Justify Center",
  property: "justify-center",
  value: "justify-center",
  input: "none",
  type: "class",
};

export const justifyCenter = {
  label: "Justify Center",
  property: "justify-center",
  value: "justify-center",
  input: "none",
  type: "class",
};

export const selfCenter = {
  label: "Self Alignment",
  property: "self-align",
  value: "self-center",
  input: "none",
  type: "class",
};
