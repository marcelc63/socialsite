import _ from "lodash";

import * as classes from "./classes.js";
import * as spacing from "./spacing.js";
import * as text from "./text.js";
import * as others from "./others.js";

export function factory() {
  return _.cloneDeep({
    ...classes,
    ...spacing,
    ...text,
    ...others,
  });
}
