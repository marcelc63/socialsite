export const padding = {
  label: "Padding",
  property: "padding",
  value: "10px",
  input: "spacing",
  meta: {
    topBottom: "10px",
    leftRight: "10px",
  },
  unit: "px",
  type: "editor",
};

export const paddingTop = {
  label: "Padding Top",
  property: "padding-top",
  value: "10px",
  input: "none",
  max: "200",
  unit: "px",
  type: "style",
};

export const paddingBottom = {
  label: "Padding Bottom",
  property: "padding-bottom",
  value: "10px",
  input: "none",
  max: "200",
  unit: "px",
  type: "style",
};

export const paddingRight = {
  label: "Padding Right",
  property: "padding-right",
  value: "10px",
  input: "none",
  max: "200",
  unit: "px",
  type: "style",
};

export const paddingLeft = {
  label: "Padding Left",
  property: "padding-left",
  value: "10px",
  input: "none",
  max: "500",
  unit: "px",
  type: "style",
};

export const paddingInput = [
  padding,
  paddingTop,
  paddingBottom,
  paddingLeft,
  paddingRight,
];

export const marginTop = {
  label: "Margin Top",
  property: "margin-top",
  value: "10px",
  input: "slider",
  max: "200",
  unit: "px",
  type: "style",
};
export const marginBottom = {
  label: "Margin Bottom",
  property: "margin-bottom",
  value: "10px",
  input: "slider",
  max: "200",
  unit: "px",
  type: "style",
};
export const marginRight = {
  label: "Margin Right",
  property: "margin-right",
  value: "10px",
  input: "slider",
  max: "200",
  unit: "px",
  type: "style",
};
export const marginLeft = {
  label: "Margin Left",
  property: "margin-left",
  value: "10px",
  input: "slider",
  max: "200",
  unit: "px",
  type: "style",
};
