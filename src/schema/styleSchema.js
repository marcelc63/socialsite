/*
Two type: class and style

Class
- Label
- Property
- Type
-- Options
--- List of Options
-- None
- Value

Style
- Label
- Property
- Type
-- Slider
--- Max
--- Min
--- unit
-- Color
-- None
-Value
*/

import { style as background } from "./styles/background.js";
import { style as header } from "./styles/header.js";
import { style as header__title } from "./styles/header__title.js";
import { style as header__description } from "./styles/header__description.js";
import { style as navigation } from "./styles/navigation.js";
import { style as navigation__link } from "./styles/navigation__link.js";
import { style as body } from "./styles/body.js";
import { style as card } from "./styles/card.js";
import { style as card__image } from "./styles/card__image.js";
import { style as card__caption } from "./styles/card__caption.js";
import { style as card__spacing } from "./styles/card__spacing.js";
import { style as link } from "./styles/link.js";
import { style as content } from "./styles/content.js";
import { style as paragraph } from "./styles/paragraph.js";
import { style as paragraph__title } from "./styles/paragraph__title.js";
import { style as paragraph__description } from "./styles/paragraph__description.js";
import { style as paragraph__content } from "./styles/paragraph__content.js";

let style = [
  background,
  header,
  header__title,
  header__description,
  navigation,
  navigation__link,
  body,
  card,
  card__image,
  card__caption,
  card__spacing,
  link,
  content,
  paragraph,
  paragraph__title,
  paragraph__description,
  paragraph__content,
];

export default style;

export const editor = [
  {
    label: "General",
    page: ["instagram", "links"],
    members: ["background", "body"],
  },
  {
    label: "Header",
    page: ["instagram", "links"],
    members: ["header", "header__title", "header__description"],
  },
  {
    label: "Navigation",
    page: ["instagram", "links"],
    members: ["navigation", "navigation__link"],
  },
  {
    label: "Card",
    page: ["instagram"],
    members: ["card", "card__spacing", "card__img", "card__caption"],
  },
  {
    label: "Link",
    page: ["links"],
    members: ["link"],
  },
  {
    label: "Paragraph",
    page: ["links"],
    members: [
      "paragraph",
      "paragraph__title",
      "paragraph__description",
      "paragraph__content",
    ],
  },
];
