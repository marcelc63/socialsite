let style = {
  label: "Image",
  component: "card__img",
  configs: [
    {
      label: "Border Radius",
      property: "border-radius",
      value: "8px",
      input: "slider",
      unit: "px",
      type: "style",
    },
    {
      label: "Width",
      property: "width",
      value: "80%",
      input: "slider",
      unit: "%",
      type: "style",
    },
  ],
};

export default style;
