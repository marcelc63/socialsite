let style = {
  label: "Navigation Link",
  component: "navigation__link",
  configs: [
    {
      label: "Font Size",
      property: "font-size",
      value: "17px",
      input: "slider",
      unit: "px",
      type: "style",
    },
    {
      label: "Font Weight",
      property: "font-weight",
      value: "243",
      input: "slider",
      unit: "",
      max: "800",
      type: "style",
    },
    {
      label: "Color",
      property: "color",
      value: "rgba(0,0,0,1)",
      input: "color",
      type: "style",
    },
    {
      label: "Padding Top",
      property: "padding-top",
      value: "0px",
      input: "slider",
      unit: "px",
      type: "style",
    },
    {
      label: "Padding Bottom",
      property: "padding-bottom",
      value: "13px",
      input: "slider",
      unit: "px",
      type: "style",
    },
    {
      label: "Padding Right",
      property: "padding-right",
      value: "5px",
      input: "slider",
      unit: "px",
      type: "style",
    },
    {
      label: "Padding Left",
      property: "padding-left",
      value: "5px",
      input: "slider",
      unit: "px",
      type: "style",
    },
  ],
};

export default style;
