let style = {
  label: "Content",
  component: "content",
  configs: [
    {
      label: "Flex",
      property: "flex",
      value: "flex",
      input: "none",
      type: "class",
    },
    {
      label: "Flex Orientation",
      property: "flex-orientation",
      value: "flex-col",
      input: "none",
      type: "class",
    },
    {
      label: "Item Aligns",
      property: "align-items",
      value: "items-center",
      input: "none",
      type: "class",
    },
    {
      label: "Width",
      property: "width",
      value: "100%",
      input: "none",
      type: "style",
    },
  ],
};

export default style;
