import { factory } from "../factory/factory.js";

let {
  flex,
  flexRow,
  flexWrap,
  alignment,
  justifyCenter,
  backgroundColor,
  backgroundGradient,
  maxWidth,
  width,
  borderRadius,
  marginTop,
  marginBottom,
  marginLeft,
  marginRight,
  paddingBottom,
  paddingRight,
  paddingTop,
  paddingLeft,
} = factory();

export const style = {
  label: "Body",
  component: "body",
  configs: [
    flex,
    flexRow,
    flexWrap,
    alignment,
    justifyCenter,
    backgroundGradient,
    maxWidth,
    width,
    borderRadius,
  ],
  mobile: [
    marginTop,
    marginBottom,
    marginLeft,
    marginRight,
    paddingBottom,
    paddingRight,
    paddingTop,
    paddingLeft,
  ],
  desktop: [
    marginTop,
    marginBottom,
    marginLeft,
    marginRight,
    paddingBottom,
    paddingRight,
    paddingTop,
    paddingLeft,
  ],
};
