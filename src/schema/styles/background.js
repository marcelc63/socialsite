import { factory } from "../factory/factory.js";

let {
  flex,
  overflow,
  alignment,
  height,
  flexCol,
  backgroundGradient,
  backgroundImage,
  backgroundRepeat,
  backgroundAttachment,
  backgroundPosition,
  backgroundSize,
  paddingBottom,
  paddingRight,
  paddingTop,
  paddingLeft,
} = factory();

export const style = {
  label: "Background",
  component: "background",
  configs: [
    flex,
    overflow,
    alignment,
    height,
    {
      ...height,
      value: "lg:min-h-screen",
      device: "desktop",
    },
    height,
    flexCol,
    backgroundGradient,
    backgroundRepeat,
    backgroundAttachment,
    backgroundPosition,
    backgroundSize,
  ],
  mobile: [paddingBottom, paddingRight, paddingTop, paddingLeft],
  desktop: [paddingBottom, paddingRight, paddingTop, paddingLeft],
};
