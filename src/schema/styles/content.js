import { factory } from "../factory/factory.js";

let { flex, flexCol, itemsCenter, width } = factory();

export const style = {
  label: "Content",
  component: "content",
  configs: [flex, flexCol, itemsCenter, width],
  mobile: [],
  desktop: [],
};
