import { factory } from "../factory/factory.js";

let {
  width,
  textAlign,
  fontSize,
  fontWeight,
  fontFamily,
  color,
  paddingBottom,
  paddingRight,
  paddingTop,
  paddingLeft,
  showBlock,
} = factory();

export const style = {
  label: "Caption",
  component: "card__caption",
  configs: [width, showBlock, textAlign, fontFamily, fontWeight, color],
  mobile: [fontSize, paddingBottom, paddingRight, paddingTop, paddingLeft],
  desktop: [fontSize, paddingBottom, paddingRight, paddingTop, paddingLeft],
};
