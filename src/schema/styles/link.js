import { factory } from "../factory/factory.js";

let {
  cursor,
  borderRadius,
  borderColor,
  borderWidth,
  backgroundGradient,
  backgroundGradientHover,
  fontSize,
  fontWeight,
  textAlign,
  color,
  width,
  marginRight,
  marginLeft,
  marginBottom,
  marginTop,
  paddingBottom,
  paddingRight,
  paddingTop,
  paddingLeft,
} = factory();

export const style = {
  label: "Link",
  component: "link",
  configs: [
    cursor,
    borderRadius,
    borderColor,
    borderWidth,
    backgroundGradient,
    backgroundGradientHover,
    fontWeight,
    textAlign,
    color,
    width,
  ],
  mobile: [
    fontSize,
    marginRight,
    marginLeft,
    marginBottom,
    marginTop,
    paddingBottom,
    paddingRight,
    paddingTop,
    paddingLeft,
  ],
  desktop: [
    fontSize,
    marginRight,
    marginLeft,
    marginBottom,
    marginTop,
    paddingBottom,
    paddingRight,
    paddingTop,
    paddingLeft,
  ],
};
