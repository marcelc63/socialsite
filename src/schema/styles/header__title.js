import { factory } from "../factory/factory.js";

let {
  flex,
  flexCol,
  backgroundGradient,
  color,
  textAlign,
  maxWidth,
  width,
  fontSize,
  fontWeight,
  borderRadius,
  paddingBottom,
  paddingRight,
  paddingTop,
  paddingLeft,
} = factory();

export const style = {
  label: "Header Title",
  component: "header__title",
  configs: [
    flex,
    flexCol,
    backgroundGradient,
    color,
    { ...textAlign, value: "center" },
    maxWidth,
    width,
    borderRadius,
    fontWeight,
  ],
  mobile: [paddingBottom, fontSize, paddingRight, paddingTop, paddingLeft],
  desktop: [paddingBottom, fontSize, paddingRight, paddingTop, paddingLeft],
};
