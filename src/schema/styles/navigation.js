import { factory } from "../factory/factory.js";

let {
  flex,
  flexRow,
  justifyCenter,
  selfCenter,
  width,
  backgroundGradient,
  borderColor,
  borderWidth,
  borderRadius,
  paddingBottom,
  paddingRight,
  paddingTop,
  paddingLeft,
  show,
} = factory();

export const style = {
  label: "Navigation",
  component: "navigation",
  configs: [
    show,
    flex,
    flexRow,
    justifyCenter,
    selfCenter,
    width,
    backgroundGradient,
    borderColor,
    borderWidth,
    borderRadius,
  ],
  mobile: [paddingBottom, paddingRight, paddingTop, paddingLeft],
  desktop: [paddingBottom, paddingRight, paddingTop, paddingLeft],
};
