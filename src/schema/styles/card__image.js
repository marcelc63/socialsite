import { factory } from "../factory/factory.js";

let { borderRadius, width, maxWidth } = factory();

export const style = {
  label: "Card Image",
  component: "card__img",
  configs: [borderRadius, width],
  mobile: [maxWidth],
  desktop: [maxWidth],
};
