import { factory } from "../factory/factory.js";

let {
  flex,
  column,
  marginTop,
  marginBottom,
  marginLeft,
  marginRight,
} = factory();

export const style = {
  label: "Card Spacing",
  component: "card__spacing",
  configs: [flex],
  mobile: [{ ...column, value: "w-full" }],
  desktop: [column],
};
