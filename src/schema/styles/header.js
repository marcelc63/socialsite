import { factory } from "../factory/factory.js";

let {
  flex,
  flexCol,
  backgroundGradient,
  maxWidth,
  width,
  borderRadius,
  paddingInput,
  paddingBottom,
  paddingRight,
  paddingTop,
  paddingLeft,
} = factory();

export const style = {
  label: "Header",
  component: "header",
  configs: [flex, flexCol, backgroundGradient, maxWidth, width, borderRadius],
  mobile: [...paddingInput],
  desktop: [paddingBottom, paddingRight, paddingTop, paddingLeft],
};
