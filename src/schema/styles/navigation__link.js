import { factory } from "../factory/factory.js";

let {
  fontSize,
  fontWeight,
  color,
  paddingBottom,
  paddingRight,
  paddingTop,
  paddingLeft,
  cursor,
} = factory();

export const style = {
  label: "Navigation Link",
  component: "navigation__link",
  configs: [fontSize, fontWeight, color, cursor],
  mobile: [paddingBottom, paddingRight, paddingTop, paddingLeft],
  desktop: [paddingBottom, paddingRight, paddingTop, paddingLeft],
};
