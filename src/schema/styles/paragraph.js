import { factory } from "../factory/factory.js";

let {
  flex,
  flexCol,
  itemsCenter,
  justify,
  width,
  paddingBottom,
  paddingRight,
  paddingTop,
  paddingLeft,
} = factory();

export const style = {
  label: "Paragraph",
  component: "paragraph",
  configs: [flex, flexCol, itemsCenter, justify, width],
  mobile: [paddingBottom, paddingRight, paddingTop, paddingLeft],
  desktop: [paddingBottom, paddingRight, paddingTop, paddingLeft],
};
