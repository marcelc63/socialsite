import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

import defaultStyle from "./schema/defaultStyle.js";
import axios from "axios";
import _ from "lodash";

function parseStyle(payload) {
  return defaultStyle.map((x) => {
    let component = payload.filter((s) => s.component === x.component);

    let result = {
      ...x,
      configs: x.configs.map((s) => {
        if (component.length !== 0 && s.input !== "none") {
          let config = component[0].configs.filter(
            (t) => t.property === s.property
          );
          if (config.length !== 0) {
            s.value = config[0].value;
            s.meta = config[0].meta;
          }
        }
        return {
          ...s,
        };
      }),
      mobile: x.mobile.map((s) => {
        if (component.length !== 0 && s.input !== "none") {
          let config = component[0].mobile.filter(
            (t) => t.property === s.property
          );
          if (config.length !== 0) {
            s.value = config[0].value;
            s.meta = config[0].meta;
          }
        }
        return {
          ...s,
        };
      }),
      desktop: x.desktop.map((s) => {
        if (component.length !== 0 && s.input !== "none") {
          let config = component[0].desktop.filter(
            (t) => t.property === s.property
          );
          if (config.length !== 0) {
            s.value = config[0].value;
            s.meta = config[0].meta;
          }
        }
        return {
          ...s,
        };
      }),
    };

    return result;
  });
}

let saveStyle = _.debounce((payload) => {
  axios.defaults.headers.common["Authorization"] = localStorage.getItem(
    "jwtToken"
  );
  axios
    .post(process.env.VUE_APP_API + `/api/site/update/design`, {
      _id: payload.site,
      style: parseStyle(payload.style),
    })
    .then((res) => {
      console.log("style updated");
      payload.notify();
    });
  return;
}, 500);

let style = _.cloneDeep(defaultStyle);

export default new Vuex.Store({
  state: {
    style: [...style],
    site: {
      site: {},
      pages: [],
      page: {},
      design: {},
      posts: [],
    },
    dashboard: {
      editMode: false,
      editorView: "mobile",
      hasPage: false,
    },
    modal: {
      mode: "",
      pageType: "",
      pageId: "",
      postType: "",
      form: {
        _id: "",
        title: "",
        link: "",
        caption: "",
        hashtags: "",
        image: "",
        textarea: "",
        type: "",
        filter: "",
        name: "",
        slug: "",
        handle: "",
      },
    },
  },
  mutations: {
    dashboard: function(state, payload) {
      if (payload.type === "navigation") {
        state.site.site.navigation = payload.navigation;
      }
      if (payload.type === "pages") {
        state.site.pages = payload.pages;
      }
    },
    modal: function(state, payload) {
      if (payload.type === "create") {
        state.modal.pageType = payload.pageType;
        state.modal.pageId = payload.pageId;
        state.modal.mode = "create";
      }
      if (payload.type === "edit") {
        state.modal.pageType = payload.pageType;
        state.modal.pageId = payload.pageId;
        state.modal.mode = "edit";
        state.modal.form = {
          _id: payload.form._id,
          title: payload.form.title,
          link: payload.form.link,
          hashtags: payload.form.hashtags,
          image: payload.form.image,
          caption: payload.form.caption,
          textarea: payload.form.textarea,
          type: payload.form.type,
          name: payload.form.name,
          slug: payload.form.slug,
          filter: payload.form.filter,
          handle: payload.form.handle,
        };
      }
      if ((payload.type = "post type")) {
        state.modal.postType = payload.postType;
      }
    },
    site: function(state, payload) {
      state.site.site = payload.site;
      state.site.pages = payload.pages;
      state.site.page = payload.page;
      state.site.design = payload.design;
      state.site.posts = payload.posts;
      state.site.navigation = payload.navigation;
    },
    style: function(state, payload) {
      state.style = parseStyle(payload.style);
    },
    editMode: function(state, payload) {
      state.dashboard.editMode = payload;
    },
    editorView: function(state, payload) {
      state.dashboard.editorView = payload;
    },
    hasPage: function(state, payload) {
      state.dashboard.hasPage = payload;
    },
  },
  actions: {
    style: function({ commit }, payload) {
      commit("style", {
        style: payload.style,
      });

      saveStyle(payload);
      return;

      if (payload.save) {
        axios.defaults.headers.common["Authorization"] = localStorage.getItem(
          "jwtToken"
        );
        axios
          .post(process.env.VUE_APP_API + `/api/site/update/design`, {
            site: payload.site,
            style: parseStyle(payload.style),
          })
          .then((res) => {
            console.log("ok");
          });
      }
    },
    siteGetData: async function({ commit }, packet) {
      let url = "";
      let pkg = {};
      let params = packet.params;
      if (params.hasPage) {
        url = `/api/page/load`;
        pkg = {
          site: params.site,
          slug: params.page,
        };
      } else {
        url = `/api/site/load`;
        pkg = {
          slug: params.site,
        };
      }

      //Get Site Info
      let siteResult = await axios.post(process.env.VUE_APP_API + url, pkg);

      //Get All Pages
      let pagesResult = await axios.get(
        process.env.VUE_APP_API +
          `/api/site/get/pages/${siteResult.data.site._id}`
      );
      let pages = pagesResult.data.pages;

      //Add Data to Payload
      let payload = {
        site: siteResult.data.site,
        page: siteResult.data.page,
        posts: siteResult.data.posts,
        design: siteResult.data.design,
        pages: pages,
      };

      console.log(payload.site.navigation);

      //Dispatch Payload
      commit("site", {
        type: "all",
        ...payload,
      });

      commit("style", {
        style: JSON.parse(payload.design.style),
      });
    },
  },
  getters: {
    site: (state) => {
      return state.site;
    },
    style: (state) => {
      return state.style;
    },
    modal: (state) => {
      return {
        pageType: state.modal.pageType,
        pageId: state.modal.pageId,
        mode: state.modal.mode,
        form: state.modal.form,
      };
    },
    pages: (state) => {
      return state.site.pages;
    },
    navigation: (state) => {
      return state.site.site.navigation;
    },
    editMode: (state) => {
      return state.dashboard.editMode;
    },
    editorView: (state) => {
      return state.dashboard.editorView;
    },
    hasPage: (state) => {
      return state.dashboard.hasPage;
    },
  },
});
