import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faChevronLeft,
  faDesktop,
  faMobile,
  faEdit,
  faCross,
  faHome,
  faUser,
  faEllipsisV,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

library.add(
  faChevronLeft,
  faDesktop,
  faMobile,
  faEdit,
  faCross,
  faHome,
  faUser,
  faEllipsisV
);

export default FontAwesomeIcon;
