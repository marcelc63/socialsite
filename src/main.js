import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./main.css";
import "./style/global.scss";

import FontAwesomeIcon from "./fontawesome.js";

Vue.component("font-awesome-icon", FontAwesomeIcon);

Vue.config.productionTip = false;

import vmodal from "vue-js-modal";
Vue.use(vmodal);

import Notifications from "vue-notification";
Vue.use(Notifications);

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
