// Builder Structure Testing
// Inspired by Flutter

let page = {
  meta: {
    // Meta stuff
  },
  children: [] //Components Here
};

let blocks = [
  {
    type: "container",
    hasChildren: true
  },
  { type: "text", hasChildren: false },
  { type: "image", hasChildren: false },
  { type: "content", hasChildren: true }
];

let component = {
  type: "", //Container, Text, Image, Slideshow, Catalogue, Navigation, Link, Icon, List
  hasChildren: "", //True or False based on component Type
  style: {
    backgroundColor: "",
    backgroundImage: "",
    fontFamily: "",
    fontColor: "",
    fontSize: "",
    fontWeight: "",
    textAlign: "",
    textDecoration: "",
    paddingTop: "",
    paddingBottom: "",
    paddingRight: "",
    paddingLeft: "",
    marginTop: "",
    marginBottom: "",
    marginRight: "",
    marginLeft: "",
    borderColor: "",
    borderSize: "",
    borderType: "",
    borderRadius: "",
    borderTop: "",
    borderBottom: "",
    borderRight: "",
    borderLeft: "",
    boxShadow: ""
  },
  content: {
    direction: "", //Row or Col
    col: "" //One, two, three, four
  },
  stream: {
    social: "",
    filter: "",
    sort: ""
  },
  children: []
};

export default {
  page,
  blocks,
  component
};
