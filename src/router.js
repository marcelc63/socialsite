import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

console.log("Screen Size", window.screen.width);
let screenWidth = window.screen.width;

const Router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: () => import("./views/Home.vue"),
      meta: {
        guest: true,
      },
    },
    {
      path: "/login",
      name: "login",
      component: () => import("./views/Login.vue"),
      meta: {
        guest: true,
      },
    },
    {
      path: "/register",
      name: "register",
      component: () => import("./views/Register.vue"),
      meta: {
        guest: true,
      },
    },
    {
      path: "/dashboard",
      name: "dashboard",
      component: () => import("./views/Dashboard.vue"),
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: "/onboard",
      name: "onboard",
      component: () => import("./views/Onboard.vue"),
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: "/showpresets",
      name: "showpresets",
      component: () => import("./views/ShowPreset.vue"),
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: "/dashboard/:site",
      name: "dashboardSite",
      component: () =>
        screenWidth > 500
          ? import("./views/integrated/IntegratedEditor.vue")
          : import("./views/integrated/IntegratedMobile.vue"),
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: "/dashboard/:site/:page",
      name: "dashboardPage",
      component: () =>
        screenWidth > 500
          ? import("./views/integrated/IntegratedEditor.vue")
          : import("./views/integrated/IntegratedMobile.vue"),
      meta: {
        requiresAuth: true,
      },
    },
  ],
});

Router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (localStorage.getItem("jwtToken") == null) {
      next({
        path: "/",
        params: { nextUrl: to.fullPath },
      });
    } else {
      next();
      let usergroup = localStorage.getItem("usergroup");
      if (to.matched.some((record) => record.meta.is_admin)) {
        if (usergroup === "admin") {
          next();
        } else {
          next({
            path: "/",
            params: { nextUrl: to.fullPath },
          });
        }
      } else {
        next();
      }
    }
  } else if (to.matched.some((record) => record.meta.guest)) {
    if (localStorage.getItem("jwtToken") == null) {
      next();
    } else {
      next({ path: "/dashboard" });
    }
  } else {
    next();
  }
});

export default Router;
