//Dependencies
const express = require("express");
const app = express();
let http = require("http").Server(app);
let port = 3007;
var path = require("path");
var cors = require("cors");

var bodyParser = require("body-parser");

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "*");
  res.header("Access-Control-Allow-Methods", "*");
  next();
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var mongoose = require("mongoose");
var credentials = require("./config.js");
mongoose.Promise = require("bluebird");
mongoose
  .connect(credentials.mongoConnection, {
    promiseLibrary: require("bluebird"),
    useNewUrlParser: true,
    auth: { authdb: "admin" },
    useFindAndModify: false,
  })
  .then(() => console.log("connection succesful"))
  .catch((err) => console.error(err));
mongoose.set("useCreateIndex", true);

//Routes
let auth = require("./routes/auth");
app.use("/auth", auth);

let api = require("./routes/api");
app.use("/api", api);

//Template and Site Views
let mustacheExpress = require("mustache-express");
app.set("views", `${__dirname}/views`);
app.set("view engine", "ejs");
// app.engine("mustache", mustacheExpress());

app.use(express.static(__dirname + "/../dist"));
app.disable("etag");

let site = require("./routes/site");
app.use("/", site);

// app.get("/dashboard", function(req, res) {
//   res.sendFile(path.join(__dirname + "/../dist/index.html"));
// });

// app.get("*", function(req, res) {
//   res.sendFile(path.join(__dirname + "/../dist/index.html"));
// });

//Initiate
http.listen(port, function() {
  console.log("listening on *:", port);
});
