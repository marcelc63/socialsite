var passport = require("passport");
var settings = require("../config/settings");
require("../config/passport")(passport);
var express = require("express");
var jwt = require("jsonwebtoken");
var router = express.Router();
var User = require("../models/User");

router.post("/register", function(req, res) {
  if (!req.body.email || !req.body.password) {
    res.json({ success: false, msg: "Please pass username and password." });
  } else {
    var newUser = new User({
      email: req.body.email,
      password: req.body.password,
      usergroup: "user"
    });
    // save the user
    newUser.save(function(err) {
      console.log("Saving User");
      if (err) {
        console.log(err);
        return res.json({ success: false, msg: "Username already exists." });
      }
      console.log("Register Success");
      res.json({ success: true, msg: "Successful created new user." });
    });
  }
});

router.post("/login", function(req, res) {
  User.findOne(
    {
      email: req.body.email.toLowerCase()
    },
    function(err, user) {
      if (err) throw err;

      if (!user) {
        res.json({
          success: false,
          msg: "Authentication failed. User not found."
        });
      } else {
        // check if password matches
        user.comparePassword(req.body.password, function(err, isMatch) {
          if (isMatch && !err) {
            // if user is found and password is right create a token
            var token = jwt.sign(user.toJSON(), settings.secret);
            // return the information including token as JSON
            res.json({
              success: true,
              token: "JWT " + token,
              usergroup: user.usergroup
            });
          } else {
            res.json({
              success: false,
              msg: "Authentication failed. Wrong password."
            });
          }
        });
      }
    }
  );
});

router.post("/logout", function(req, res) {
  req.logout();
  res.json({ success: true });
});

module.exports = router;
