var express = require("express");
var router = express.Router();
var mongoose = require("mongoose");
var Model = require("../models/Idol.js");
var Rank = require("../models/Rank.js");

router.get("/all", async function(req, res) {
  Model.find({}, function(err, docs) {
    let package = docs.map(x => {
      return {
        profile: x.profile,
        stats: x.stats
      };
    });
    res.json(package);
  });
});

router.get("/rank", async function(req, res) {
  Rank.find({})
    .populate("profile")
    .exec(function(err, docs) {
      let package = {
        ...docs,
        profile: docs.profile.profile
      };
      res.json(package);
    });
});

module.exports = router;
