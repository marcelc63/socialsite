var express = require("express");
var router = express.Router();
var mongoose = require("mongoose");
var Model = require("../models/Photos.js");
var Rank = require("../models/Rank.js");
let scraper = require("../scraper/instagram/index.js");

router.get("/all", async function(req, res) {
  //   idols.forEach(x => {
  //     console.log(x)
  //   });
  async function getIdol(list, count = 0) {
    let instagram = list[count].Instagram;
    // console.log(instagram);
    // return;
    let payload = await scraper(instagram, "live");
    // console.log(payload);
    // return;
    Model.create(payload, function(err, post) {
      if (err) return console.log(err);
      // console.log("done");
      //   res.json(post);
      if (list.length === count + 1 || count === 9) {
        res.send("done");
      } else {
        getIdol(list, count + 1);
      }
    });
  }

  getIdol(idols);

  //   res.send(idols);
});

router.get("/rank", async function(req, res) {
  Model.find({}, function(err, post) {
    post.forEach(x => {
      let photos = x.photos
        .sort((a, b) => parseInt(b.likes) - parseInt(a.likes))
        .slice(0, 9);

      let payload = {
        name: x.profile.username,
        profile: x._id,
        photos,
        stats: {
          likes: x.photos.reduce((a, b) => a + parseInt(b.likes), 0),
          comments: x.photos.reduce((a, b) => a + parseInt(b.comments), 0),
          followers: parseInt(x.profile.followers),
          following: parseInt(x.profile.following),
          posts: parseInt(x.profile.posts)
        }
      };
      console.log(payload);
      Rank.create(payload, function(err, post) {
        if (err) return console.log(err);
        // console.log("done");
        //   res.json(post);
        console.log("added");
      });
    });
    // res.json(post);
    res.send("hi");
  });
});

router.get("/:account", async function(req, res) {
  let payload = await scraper(req.params.account);
  console.log(payload);
  Model.create(payload, function(err, post) {
    if (err) return console.log(err);
    // console.log("done");
    //   res.json(post);
    res.send("done");
  });
});

module.exports = router;
