var express = require("express");
var router = express.Router();
var mongoose = require("mongoose");
var Model = require("../models/Photos.js");
let scraper = require("../scraper/instagram/index.js");
let _ = require("lodash");

router.get("/scrape/:username", async function(req, res) {
  let docs = await Model.find({ "profile.username": req.params.username });
  if (docs.length > 0) {
    console.log("exist");
  } else {
    let data = await scraper(req.params.username, 20);
    let allHashtags = [];

    let photos = data.photos.map(photo => {
      let hashtags = photo.caption
        .split(" ")
        .map(x => {
          if (x.substr(0, 1) === "#") {
            x = x.replace("#", "");
            x = x.replace(",", "");
            x = x.toLowerCase();
            return x;
          }
          return false;
        })
        .filter(x => x);
      allHashtags = allHashtags.concat(hashtags);
      return {
        ...photo,
        hashtags: [...hashtags]
      };
    });
    let model = new Model({
      profile: {
        biography: data.profile.biography,
        full_name: data.profile.full_name,
        username: data.profile.username,
        profile_pic_url: data.profile.profile_pic_url,
        profile_pic_url_hd: data.profile.profile_pic_url_hd,
        followers: data.profile.followers,
        following: data.profile.following,
        posts: data.profile.posts,
        external_url: data.profile.external_url,
        hashtags: [..._.uniq(allHashtags)]
      },
      photos: [...photos]
    });
    let newModel = await model.save();
  }
  res.json({ success: true, msg: "Photos Saved" });
  // console.log(photos);
});

router.get("/photos/:username", async function(req, res) {
  console.log(req.params.username);
  let docs = await Model.findOne({ "profile.username": req.params.username });
  //   console.log(docs);
  //   return docs;
  res.json({ success: true, data: docs.photos });
});

router.get("/:username/hashtags/:hashtag", async function(req, res) {
  console.log(req.params.username);
  let docs = await Model.findOne({
    "profile.username": req.params.username
  });
  let photos = docs.photos.filter(x => x.hashtags.includes(req.params.hashtag));
  console.log(docs.photos);
  //   return docs;
  res.json({ success: true, data: photos });
});

router.get("/:username/hashtags", async function(req, res) {
  console.log(req.params.username);
  let docs = await Model.findOne({ "profile.username": req.params.username });
  //   console.log(docs);
  //   return docs;
  res.json({ success: true, data: docs.profile.hashtags });
});

module.exports = router;
