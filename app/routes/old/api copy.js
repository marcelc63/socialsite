var express = require("express");
var router = express.Router();
var mongoose = require("mongoose");
var Site = require("../models/Site.js");
var Page = require("../models/Page.js");
var Photo = require("../models/Photo.js");
let _ = require("lodash");
let instagram = require("../scraper/instagram.js");
var slugify = require("slugify");
slugify.extend({ ".": "-" });
var passport = require("passport");
require("../config/passport")(passport);
var axios = require("axios");
var uniqid = require("uniqid");

function getToken(headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(" ");
    if (parted.length === 2) {
      return parted[1];
    } else {
      return null;
    }
  } else {
    return null;
  }
}

//Sites
/*
Create Site - /site/create
Get All Pages in site /site/get/pages
Get Site info only /site/get/info
Reorder Navigation /site/update/navigation
Get site navigation /site/get/navigation
Update Site Design /site/update/design
Get Site with Pages and Photos /site/load/:slug
*/
router.post(
  "/site/create",
  passport.authenticate("jwt", { session: false }),
  async function(req, res) {
    //Authenticate
    if (!getToken(req.headers)) {
      return res.status(403).send({ success: false, msg: "Unauthorized." });
    }
    //Get Body params
    let body = req.body;

    //Scrape Instagram
    let resultIG = await instagram.scrape(body.handle);

    //Create Site Model
    let site = new Site({
      name: body.name,
      design: {
        type: "style",
        style: JSON.stringify(body.style),
      },
      social: {
        platform: "instagram",
        handle: body.handle,
        filter: "",
      },
      hashtags: resultIG.data.profile.hashtags,
      owner: req.user._id,
    });
    await site.save();

    //Map Posts
    let posts = resultIG.data.photos.map((x) => {
      return {
        tag: x.id,
        title: "",
        image: x.thumbnail_src,
        caption: x.caption,
        hashtags: x.hashtags,
        textarea: "",
        link: "",
      };
    });

    //Create Page for Site
    let page = new Page({
      name: "Home",
      site: site._id,
      slug: "home",
      social: {
        platform: "instagram",
        handle: body.handle,
        filter: "",
      },
      design: {
        type: "style",
        style: JSON.stringify(body.style),
      },
      posts: [...posts],
      type: "instagram",
      owner: req.user._id,
    });
    await page.save();

    //Site Page as Home
    site.home = page._id;
    //Add Page to Navigation
    site.navigation.push(page._id);
    //Save Site
    await site.save();

    //Return OK
    res.json({
      success: "ok",
    });
  }
);
router.get("/site/get/pages/:id", async function(req, res) {
  //Get Params
  let params = req.params;

  //Get Pages
  let pages = await Page.find({
    site: params.id,
  });

  //Return
  res.json({
    pages: pages,
  });
});
router.get("/site/get/info/:slug", async function(req, res) {
  //Get Params
  let params = req.params;

  //Get Site
  let site = await Site.findOne({
    slug: params.slug,
  });

  //Return
  res.json({
    site: site,
  });
});
router.get("/site/get/navigation/:id", async function(req, res) {
  //Get Params
  let params = req.params;

  //Get Site
  let site = await Site.findOne({
    slug: params.slug,
  });

  //Return
  res.json({
    navigation: site.navigation,
  });
});
router.get(
  "/site/get/all",
  passport.authenticate("jwt", { session: false }),
  async function(req, res) {
    //Authenticate
    if (!getToken(req.headers)) {
      return res.status(403).send({
        success: false,
        msg: "Unauthorized.",
      });
    }

    //Get Site
    let sites = await Site.find({ owner: req.user._id });

    //Return
    res.json({
      sites: sites,
    });
  }
);
router.post(
  "/site/update/navigation",
  passport.authenticate("jwt", { session: false }),
  async function(req, res) {
    //Authenticate
    if (!getToken(req.headers)) {
      return res.status(403).send({
        success: false,
        msg: "Unauthorized.",
      });
    }

    //Get Body params
    let body = req.body;

    //Find Site and update navigation
    let site = await Site.findOne({
      _id: body._id,
      owner: req.user._id,
    });
    site.navigation = body.navigation;
    await site.save();

    //Return OK
    res.json({ success: "ok" });
  }
);
router.post(
  "/site/update/design",
  passport.authenticate("jwt", { session: false }),
  async function(req, res) {
    console.log("Design Updated");

    //Authenticate
    if (!getToken(req.headers)) {
      return res.status(403).send({
        success: false,
        msg: "Unauthorized.",
      });
    }

    //Get Body params
    let body = req.body;

    //Find site and update
    let site = await Site.findOne({
      slug: body.site,
      owner: req.user._id,
    });
    site.design.style = JSON.stringify(body.style);
    await site.save();

    //Return OK
    res.json({
      success: "ok",
    });
  }
);
router.post(
  "/site/delete",
  passport.authenticate("jwt", { session: false }),
  async function(req, res) {
    //Authenticate
    if (!getToken(req.headers)) {
      return res.status(403).send({
        success: false,
        msg: "Unauthorized.",
      });
    }

    //Get Body params
    let body = req.body;

    //Get Site
    let site = await Site.findOne({
      _id: body._id,
      owner: req.user._id,
    });

    //Get Page and Remove
    await Page.deleteMany({
      site: site._id,
    });

    //Remove Site
    await site.remove();

    //Return
    res.json({
      success: "ok",
    });
  }
);
router.post("/site/load/", async function(req, res) {
  //Get Body params
  let body = req.body;

  //Get Site
  let site = await Site.findOne({
    slug: body.slug,
  });
  //Get Page
  let page = await Page.findOne({
    _id: site.home,
  });

  //Return
  res.json({
    page: page,
    posts: page.posts,
    site: site,
    design: site.design,
  });
});

//Pages
/*
Create Page /page/create
Edit Page /page/edit/:id
Delete Page /page/delete/:id
Update Page Design /page/update/design
Set page as home /page/update/home
Get page with photos /page/load/:id
Get page info only /page/get/info
*/

router.post(
  "/page/create",
  passport.authenticate("jwt", { session: false }),
  async function(req, res) {
    //Authenticate
    if (!getToken(req.headers)) {
      return res.status(403).send({
        success: false,
        msg: "Unauthorized.",
      });
    }

    //Get Body params
    let body = req.body;

    //Find site and update
    let site = await Site.findOne({
      _id: body.site,
      owner: req.user._id,
    });

    let posts = [];

    if (body.type === "instagram") {
      let photos = await Photo.findOne({
        slug: slugify(site.social.handle),
      });

      // Filter
      let filteredPhotos = [...photos.photos];
      if (body.filter !== "") {
        filteredPhotos = photos.photos.filter((x) => {
          return x.hashtags.includes(body.filter);
        });
      }

      //Map Posts
      posts = filteredPhotos.map((x) => {
        return {
          tag: x.id,
          title: "",
          image: x.thumbnail_src,
          caption: x.caption,
          hashtags: x.hashtags,
          textarea: "",
          link: "",
        };
      });
    }
    if (body.type === "links") {
      posts = [
        {
          tag: uniqid(),
          title: body.title,
          image: "",
          caption: "",
          hashtags: "",
          textarea: "",
          link: body.link,
        },
      ];
    }

    console.log(posts);

    //Create Page Model
    let page = new Page({
      name: body.name,
      site: body.site,
      slug: slugify(body.name).toLowerCase(),
      social: {
        platform: "instagram",
        handle: site.social.handle,
        filter: body.filter,
      },
      design: {
        type: "style",
        style: JSON.stringify(body.style),
      },
      posts: [...posts],
      type: body.type,
      owner: req.user._id,
    });
    //Save Model
    await page.save();

    //Add Page to Site Navigation
    // let site = await Site.findOne({ _id: body.site });
    site.navigation.push(page._id);
    await site.save();

    //Return OK
    res.json({
      success: "ok",
    });
    //Scrape Instagram Handle for Page
    // await instagram.scrape(req.body.handle);
  }
);
router.post(
  "/page/edit",
  passport.authenticate("jwt", { session: false }),
  async function(req, res) {
    //Authenticate
    if (!getToken(req.headers)) {
      return res.status(403).send({
        success: false,
        msg: "Unauthorized.",
      });
    }

    //Get Body params
    let body = req.body;

    //Find Page and Save edits
    let page = await Page.findOne({
      _id: body._id,
      owner: req.user._id,
    });

    page.name = body.name;
    page.social.handle = body.handle;
    page.social.filter = body.filter;
    await page.save();

    //Return OK
    res.json({
      success: "ok",
    });
  }
);
router.post(
  "/page/delete",
  passport.authenticate("jwt", { session: false }),
  async function(req, res) {
    //Authenticate
    if (!getToken(req.headers)) {
      return res.status(403).send({
        success: false,
        msg: "Unauthorized.",
      });
    }

    //Get Body params
    let body = req.body;

    //Get Page and Edit
    let page = await Page.findOne({
      _id: body._id,
      owner: req.user._id,
    });

    //Get Site
    let site = await Site.findOne({
      _id: page.site,
      owner: req.user._id,
    });
    //Remove from navigation and save
    site.navigation = site.navigation.filter((x) => x != page._id);
    await site.save();

    //Remove Page
    await page.remove();

    //Return
    res.json({
      success: "ok",
    });
  }
);
router.post(
  "/page/update/design",
  passport.authenticate("jwt", { session: false }),
  async function(req, res) {
    //Authenticate
    if (!getToken(req.headers)) {
      return res.status(403).send({
        success: false,
        msg: "Unauthorized.",
      });
    }

    //Get Body params
    let body = req.body;

    //Get Page and Edit
    let page = await Page.findOne({
      _id: body._id,
      owner: req.user._id,
    });
    page.design.style = JSON.stringify(body.style);
    await page.save();

    //Return
    res.json({ success: "ok" });
  }
);
router.post(
  "/page/update/home",
  passport.authenticate("jwt", { session: false }),
  async function(req, res) {
    //Authenticate
    if (!getToken(req.headers)) {
      return res.status(403).send({
        success: false,
        msg: "Unauthorized.",
      });
    }

    //Get Body params
    let body = req.body;

    //Get Site Information and edit home
    let site = await Site.findOne({
      _id: body.site,
      owner: req.user._id,
    });
    site.home = body.page;
    await site.save();

    //Return
    res.json({ success: "ok" });
  }
);
router.get("/page/get/info/:id", async function(req, res) {});
router.post("/page/load", async function(req, res) {
  //Get params
  let body = req.body;

  //Get Site
  let site = await Site.findOne({
    slug: body.site,
  });
  //Get Page
  let page = await Page.findOne({
    slug: body.slug,
    site: site._id,
  });

  //Return
  res.json({
    page: page,
    posts: page.posts,
    site: site,
    design: site.design,
  });
});

//Posts
// Create posts
// Get single post data /post/load/:id
router.post(
  "/post/create",
  passport.authenticate("jwt", { session: false }),
  async function(req, res) {
    //Authenticate
    if (!getToken(req.headers)) {
      return res.status(403).send({
        success: false,
        msg: "Unauthorized.",
      });
    }

    //Get Body params
    let body = req.body;

    //Setup Post
    let post = {
      tag: uniqid(),
      ...body.post,
    };

    //Get Page and Edit
    let page = await Page.updateOne(
      {
        _id: body._id,
        owner: req.user._id,
      },
      {
        $push: {
          posts: post,
        },
      }
    );

    //Return
    res.json({
      success: "ok",
    });
  }
);
router.post(
  "/post/edit",
  passport.authenticate("jwt", { session: false }),
  async function(req, res) {
    //Authenticate
    if (!getToken(req.headers)) {
      return res.status(403).send({
        success: false,
        msg: "Unauthorized.",
      });
    }

    //Get Body params
    let body = req.body;

    // console.log(body);

    //Find Page and Save edits
    let post = await Page.updateOne(
      {
        _id: body.page,
        owner: req.user._id,
        "posts._id": body._id,
      },
      {
        $set: {
          "posts.$.title": body.title,
          "posts.$.image": body.image,
          "posts.$.caption": body.caption,
          "posts.$.hashtags": body.hashtags,
          "posts.$.textarea": body.textarea,
          "posts.$.link": body.link,
        },
      }
    );

    //Return OK
    res.json({
      success: "ok",
    });
  }
);
router.post(
  "/post/delete",
  passport.authenticate("jwt", { session: false }),
  async function(req, res) {
    //Authenticate
    if (!getToken(req.headers)) {
      return res.status(403).send({
        success: false,
        msg: "Unauthorized.",
      });
    }

    //Get Body params
    let body = req.body;

    // console.log(body);

    //Find Page and Save edits
    let post = await Page.updateOne(
      {
        _id: body.page,
        owner: req.user._id,
      },
      {
        $pull: {
          posts: {
            _id: body._id,
          },
        },
      }
    );

    console.log(post);

    //Return OK
    res.json({
      success: "ok",
    });
  }
);
router.post("/post/load", async function(req, res) {});

router.post("/search", async function(req, res) {
  let body = req.body;
  console.log("search");
  let result = await axios({
    url: `https://www.instagram.com/web/search/topsearch/`,
    method: "get",
    mode: "no-cors",
    params: {
      context: "blended",
      query: body.search,
    },
    headers: {
      "User-Agent":
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0",
    },
  });
  console.log(result);
  res.json({
    users: result.data.users.filter((x) => x.user.is_private === false),
  });
});
module.exports = router;
