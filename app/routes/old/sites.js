var express = require("express");
var router = express.Router();
var mongoose = require("mongoose");
var Model = require("../models/Sites.js");
var Page = require("../models/Pages.js");
var Photos = require("../models/Photos.js");
let _ = require("lodash");
let instagram = require("./instagramv1.js");
var slugify = require("slugify");
slugify.extend({ ".": "-" });

router.post("/create", async function(req, res) {
  console.log(req.body);
  let model = new Model({
    name: req.body.name,
    design: {
      type: "style",
      style: JSON.stringify(req.body.style)
    }
  });
  let site = await model.save();

  let page = new Page({
    name: "Home",
    site: site._id,
    social: {
      platform: "instagram",
      handle: req.body.handle,
      filter: ""
    }
  });
  await page.save();

  site.home = page._id;
  site.navigation.push(page._id);
  await site.save();

  await instagram.scrape(req.body.handle);

  res.send("ok");
});

router.post("/create/page", async function(req, res) {
  console.log(req.body);
  let model = new Page({
    name: req.body.name,
    site: req.body.site,
    social: {
      platform: "instagram",
      handle: req.body.handle,
      filter: req.body.filter
    },
    design: {
      type: "style",
      style: JSON.stringify(req.body.style)
    }
  });
  await model.save();

  let site = await Model.findOne({ _id: req.body.site });
  site.navigation.push(model._id);
  await site.save();

  res.send("ok");
  await instagram.scrape(req.body.handle);
});

router.post("/edit/page", async function(req, res) {
  console.log(req.body);

  let page = await Page.findOne({ _id: req.body.page._id });
  page.name = req.body.page.name;
  page.social.handle = req.body.page.handle;
  page.social.filter = req.body.page.filter;
  await page.save();
  res.send({ success: "ok" });
});

router.post("/nav/reorder", async function(req, res) {
  let site = await Model.findOne({ _id: req.body._id });
  site.navigation = req.body.navigation;
  await site.save();
  res.send({ success: "ok" });
});

router.get("/getAll", async function(req, res) {
  let all = await Model.find({});
  // console.log(all);
  res.send(all);
});

router.post("/update/page/style", async function(req, res) {
  let site = await Model.findOne({ slug: req.body.slug });
  site.design.style = JSON.stringify(req.body.style);
  await site.save();
  console.log("save");
  res.send("ok");
});

router.get("/getPages/:id", async function(req, res) {
  let all = await Page.find({ site: req.params.id });
  res.json(all);
});

router.post("/setAsHome", async function(req, res) {
  let site = await Model.findOne({ _id: req.body.site });
  // console.log(site);
  site.home = req.body.page;
  await site.save();
  res.json({ success: "ok" });
});

router.get("/get/post/:slug/:post", async function(req, res) {
  console.log("2", req.params.post);
  let site = await Model.findOne({
    slug: req.params.slug
  });

  let page = await Page.findOne({
    _id: site.home
  });

  let photos = await Photos.findOne({
    slug: slugify(page.social.handle)
  });

  let photo = photos.photos.filter(x => x.id === req.params.post)[0];

  // if (page.social.filter !== "") {
  //   photos.photos = photos.photos.filter(x => {
  //     return x.hashtags.includes(page.social.filter);
  //   });
  // }

  console.log("POST", photo);

  res.json({
    photo: photo
  });
});

router.get("/get/site/:slug", async function(req, res) {
  // console.log("2", req.params.slug);
  let site = await Model.findOne({
    slug: req.params.slug
  });

  let page = await Page.findOne({
    _id: site.home
  });

  let photos = await Photos.findOne({
    slug: slugify(page.social.handle)
  });

  if (page.social.filter !== "") {
    photos.photos = photos.photos.filter(x => {
      return x.hashtags.includes(page.social.filter);
    });
  }

  // console.log("a", photos);

  res.json({
    page: page,
    photos: photos,
    site: site,
    design: site.design
  });
});

router.get("/get/site/info/:slug", async function(req, res) {
  console.log(req.params.slug);
  let site = await Model.findOne({
    slug: req.params.slug
  });
  res.json({
    site: site
  });
});

router.get("/get/page/:slug", async function(req, res) {
  console.log("22", req.params.slug);
  let all = await Page.findOne({
    slug: req.params.slug
  });

  let site = await Model.findOne({
    _id: all.site
  });

  let photos = await Photos.findOne({
    slug: slugify(all.social.handle)
  });

  if (all.social.filter !== "") {
    photos.photos = photos.photos.filter(x => {
      return x.hashtags.includes(all.social.filter);
    });
  }

  // console.timeLog(photos);
  res.json({
    page: all,
    photos: photos,
    site: site,
    design: site.design
  });
});

module.exports = router;
