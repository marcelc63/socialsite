let express = require("express");
let router = express.Router();
let slugify = require("slugify");
slugify.extend({ ".": "-" });
let passport = require("passport");
require("../config/passport")(passport);
let axios = require("axios");

function getToken(headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(" ");
    if (parted.length === 2) {
      return parted[1];
    } else {
      return null;
    }
  } else {
    return null;
  }
}

//Import APIS
require("./api/site.js")(router, getToken);
require("./api/page.js")(router, getToken);
require("./api/post.js")(router, getToken);
require("./presets/index.js")(router, getToken);

//IG Search Module
router.post("/search", async function(req, res) {
  let body = req.body;
  console.log("search");
  let result = await axios({
    url: `https://www.instagram.com/web/search/topsearch/`,
    method: "get",
    mode: "no-cors",
    params: {
      context: "blended",
      query: body.search,
    },
  });
  console.log("search result", result.data);
  let users = [];
  if (Array.isArray(result.data.users)) {
    users = result.data.users.filter((x) => x.user.is_private === false);
  }

  res.json({
    users,
  });
});

//Upload Images
let multer = require("multer");
let storage = multer.memoryStorage();
let upload = multer({ storage: storage });
var cors = require("cors");
let credentials = require("../config.js");

router.post("/image/upload", upload.single("image"), cors(), function(
  req,
  res
) {
  let image = req.file.buffer.toString("base64");
  axios({
    method: "post",
    url: "https://api.imgur.com/3/image",
    data: {
      image: image,
      album: "y6Y7amj",
      type: "base64",
    },
    headers: {
      Authorization: credentials.imgur.clientId,
      Authorization: credentials.imgur.brearer,
    },
  })
    .then((packet) => {
      let url = packet.data.data.link;
      res.send({ link: url });
    })
    .catch((error) => console.log(error));
});

module.exports = router;
