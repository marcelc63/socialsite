var Site = require("../../models/Site.js");
var Page = require("../../models/Page.js");
var Photo = require("../../models/Photo.js");
let _ = require("lodash");
let instagram = require("../../scraper/instagramAlternative.js");
var slugify = require("slugify");
slugify.extend({ ".": "-" });
var passport = require("passport");
require("../../config/passport")(passport);
var axios = require("axios");
var uniqid = require("uniqid");

let api = async function(router, getToken) {
  //Sites
  /*
    Create Site - /site/create
    Get All Pages in site /site/get/pages
    Get Site info only /site/get/info
    Reorder Navigation /site/update/navigation
    Get site navigation /site/get/navigation
    Update Site Design /site/update/design
    Get Site with Pages and Photos /site/load/:slug
    */

  router.post(
    "/site/create",
    passport.authenticate("jwt", {
      session: false,
    }),
    async function(req, res) {
      //Authenticate
      if (!getToken(req.headers)) {
        return res.status(403).send({
          success: false,
          msg: "Unauthorized.",
        });
      }
      //Get Body params
      let body = req.body;

      //Scrape Instagram
      let resultIG = await instagram.scrape(body.handle);

      //Create Site Model
      let site = new Site({
        name: body.name,
        design: {
          type: "style",
          style: JSON.stringify(body.style),
        },
        social: {
          platform: "instagram",
          handle: body.handle,
          filter: "",
        },
        description: body.description,
        hashtags: resultIG.data.profile.hashtags,
        owner: req.user._id,
      });
      await site.save();

      if (body.slug !== "") {
        site.slug = body.slug;
        await site.save();
      }

      //Map Posts
      let posts = resultIG.data.photos.map((x, i) => {
        return {
          tag: x.id,
          title: "",
          image: x.thumbnail_src,
          caption: x.caption,
          hashtags: x.hashtags,
          textarea: "",
          link: "",
          type: "instagram",
          order: i,
        };
      });

      //Create Page for Site
      let page = new Page({
        name: "Home",
        site: site._id,
        slug: "home",
        social: {
          platform: "instagram",
          handle: body.handle,
          filter: "",
        },
        design: {
          type: "style",
          style: JSON.stringify(body.style),
        },
        posts: [...posts],
        type: "instagram",
        owner: req.user._id,
      });
      await page.save();

      //Site Page as Home
      site.home = page._id;
      //Add Page to Navigation
      site.navigation.push(page._id);
      //Save Site
      await site.save();

      //Return OK
      res.json({
        success: "ok",
      });
    }
  );

  router.post(
    "/site/create/new",
    passport.authenticate("jwt", {
      session: false,
    }),
    async function(req, res) {
      //Authenticate
      if (!getToken(req.headers)) {
        return res.status(403).send({
          success: false,
          msg: "Unauthorized.",
        });
      }
      //Get Body params
      let body = req.body;
      console.log(body);
      let social = [];
      let hasIG = body.handle !== undefined ? true : false;
      let hasLink = body.linkTitle !== undefined ? true : false;

      if (hasIG) {
        social = [
          {
            platform: "instagram",
            handle: body.handle,
          },
        ];
      }

      //Scrape Instagram
      let resultIG = await instagram.scrape(body.handle);

      //Create Site Model
      let site = new Site({
        name: body.name,
        design: {
          type: "style",
          style: body.style,
        },
        description: body.description,
        logo: body.logo,
        social: social,
        hashtags: resultIG.data.profile.hashtags,
        owner: req.user._id,
      });
      await site.save();

      if (body.slug !== "") {
        site.slug = body.slug;
        await site.save();
      }

      //Map Posts
      let posts = [
        {
          tag: "",
          title: "Welcome",
          image: "",
          caption: "",
          hashtags: "",
          textarea: `This is the landing page of ${body.name}`,
          link: "",
          type: "paragraph",
          order: 0,
        },
      ];
      if (hasLink) {
        posts.push({
          tag: "",
          title: body.linkTitle,
          image: "",
          caption: "",
          hashtags: "",
          textarea: `This is the landing page of ${body.name}`,
          link: body.linkUrl,
          type: "link",
          order: 1,
        });
      }

      //Create Page for Site
      let page = new Page({
        name: "Home",
        site: site._id,
        slug: "home",
        social: {
          platform: "",
          handle: "",
          filter: "",
        },
        design: {
          type: "style",
          style: body.style,
        },
        posts: [...posts],
        type: "links",
        owner: req.user._id,
      });
      await page.save();
      //Site Page as Home
      site.home = page._id;
      //Add Page to Navigation
      site.navigation.push(page._id);

      if (hasIG) {
        let postsIG = resultIG.data.photos.map((x, i) => {
          return {
            tag: x.id,
            title: "",
            image: x.thumbnail_src,
            caption: x.caption,
            hashtags: x.hashtags,
            textarea: "",
            link: "",
            type: "instagram",
            order: i,
          };
        });

        //Create Page for Site
        let pageIG = new Page({
          name: "Gallery",
          site: site._id,
          slug: "gallery",
          social: {
            platform: "instagram",
            handle: body.handle,
            filter: "",
          },
          design: {
            type: "style",
            style: body.style,
          },
          posts: [...postsIG],
          type: "instagram",
          owner: req.user._id,
        });
        await pageIG.save();
        site.navigation.push(pageIG._id);
      }

      //Save Site
      await site.save();

      //Return OK
      res.json({
        success: "ok",
      });
    }
  );

  router.get("/site/get/pages/:id", async function(req, res) {
    //Get Params
    let params = req.params;

    //Get Pages
    let pages = await Page.find({
      site: params.id,
    });

    //Return
    res.json({
      pages: pages,
    });
  });
  router.get("/site/get/info/:slug", async function(req, res) {
    //Get Params
    let params = req.params;

    //Get Site
    let site = await Site.findOne({
      slug: params.slug,
    });

    //Return
    res.json({
      site: site,
    });
  });
  router.get("/site/get/navigation/:id", async function(req, res) {
    //Get Params
    let params = req.params;

    //Get Site
    let site = await Site.findOne({
      slug: params.slug,
    });

    //Return
    res.json({
      navigation: site.navigation,
    });
  });
  router.get(
    "/site/get/all",
    passport.authenticate("jwt", {
      session: false,
    }),
    async function(req, res) {
      //Authenticate
      if (!getToken(req.headers)) {
        return res.status(403).send({
          success: false,
          msg: "Unauthorized.",
        });
      }

      //Get Site
      let sites = await Site.find({
        owner: req.user._id,
      });

      //Return
      res.json({
        sites: sites,
      });
    }
  );
  router.post(
    "/site/update/navigation",
    passport.authenticate("jwt", {
      session: false,
    }),
    async function(req, res) {
      //Authenticate
      if (!getToken(req.headers)) {
        return res.status(403).send({
          success: false,
          msg: "Unauthorized.",
        });
      }

      //Get Body params
      let body = req.body;

      //Find Site and update navigation
      let site = await Site.findOne({
        _id: body._id,
        owner: req.user._id,
      });

      console.log(body.navigation);
      site.navigation = body.navigation;
      await site.save();

      //Return OK
      res.json({
        success: "ok",
      });
    }
  );
  router.post(
    "/site/update/design",
    passport.authenticate("jwt", {
      session: false,
    }),
    async function(req, res) {
      console.log("Design Updated");

      //Authenticate
      if (!getToken(req.headers)) {
        return res.status(403).send({
          success: false,
          msg: "Unauthorized.",
        });
      }

      //Get Body params
      let body = req.body;

      //Find site and update
      let site = await Site.findOne({
        _id: body._id,
        owner: req.user._id,
      });
      site.design.style = JSON.stringify(body.style);
      await site.save();

      //Return OK
      res.json({
        success: "ok",
      });
    }
  );
  router.post(
    "/site/delete",
    passport.authenticate("jwt", {
      session: false,
    }),
    async function(req, res) {
      //Authenticate
      if (!getToken(req.headers)) {
        return res.status(403).send({
          success: false,
          msg: "Unauthorized.",
        });
      }

      //Get Body params
      let body = req.body;

      //Get Site
      let site = await Site.findOne({
        _id: body._id,
        owner: req.user._id,
      });

      //Get Page and Remove
      await Page.deleteMany({
        site: site._id,
      });

      //Remove Site
      await site.remove();

      //Return
      res.json({
        success: "ok",
      });
    }
  );
  router.post(
    "/site/set/home",
    passport.authenticate("jwt", {
      session: false,
    }),
    async function(req, res) {
      //Authenticate
      if (!getToken(req.headers)) {
        return res.status(403).send({
          success: false,
          msg: "Unauthorized.",
        });
      }

      //Get Body params
      let body = req.body;

      let site = await Site.findOne({
        _id: body.site,
      });
      site.home = body.page;
      await site.save();
      res.json({
        success: "ok",
      });
    }
  );
  router.post("/site/load/", async function(req, res) {
    //Get Body params
    let body = req.body;

    //Get Site
    let site = await Site.findOne({
      slug: body.slug,
    });
    //Get Page
    let page = await Page.findOne({
      _id: site.home,
    });

    // console.log(page.posts.sort((a, b) => b.order - a.order, 0));

    //Return
    res.json({
      page: page,
      posts: page.posts.sort((a, b) => b.order - a.order, 0),
      site: site,
      design: site.design,
    });
  });
};
module.exports = api;
