var Site = require("../../models/Site.js");
var Page = require("../../models/Page.js");
var Photo = require("../../models/Photo.js");
let _ = require("lodash");
let instagram = require("../../scraper/instagram.js");
var slugify = require("slugify");
slugify.extend({ ".": "-" });
var passport = require("passport");
require("../../config/passport")(passport);
var axios = require("axios");
var uniqid = require("uniqid");

let api = async function(router, getToken) {
  //Posts
  // Create posts
  // Get single post data /post/load/:id
  router.post(
    "/post/create",
    passport.authenticate("jwt", {
      session: false,
    }),
    async function(req, res) {
      //Authenticate
      if (!getToken(req.headers)) {
        return res.status(403).send({
          success: false,
          msg: "Unauthorized.",
        });
      }

      //Get Body params
      let body = req.body;

      let getPage = await Page.findOne({
        _id: body._id,
        owner: req.user._id,
      });
      let lastOrder =
        getPage.posts.length === 0
          ? 0
          : getPage.posts.sort((a, b) => a.order - b.order, 0).slice(-1)[0]
              .order + 1;

      //Setup Post
      let post = {
        tag: uniqid(),
        ...body.post,
        order: lastOrder,
      };

      //Get Page and Edit
      let page = await Page.updateOne(
        {
          _id: body._id,
          owner: req.user._id,
        },
        {
          $push: {
            posts: post,
          },
        }
      );

      //Return
      res.json({
        success: "ok",
      });
    }
  );
  router.post(
    "/post/edit",
    passport.authenticate("jwt", {
      session: false,
    }),
    async function(req, res) {
      //Authenticate
      if (!getToken(req.headers)) {
        return res.status(403).send({
          success: false,
          msg: "Unauthorized.",
        });
      }

      //Get Body params
      let body = req.body;

      // console.log(body);

      //Find Page and Save edits
      let post = await Page.updateOne(
        {
          _id: body.page,
          owner: req.user._id,
          "posts._id": body._id,
        },
        {
          $set: {
            "posts.$.title": body.title,
            "posts.$.image": body.image,
            "posts.$.caption": body.caption,
            "posts.$.hashtags": body.hashtags,
            "posts.$.textarea": body.textarea,
            "posts.$.link": body.link,
          },
        }
      );

      //Return OK
      res.json({
        success: "ok",
      });
    }
  );
  router.post(
    "/post/reorder",
    passport.authenticate("jwt", {
      session: false,
    }),
    async function(req, res) {
      //Authenticate
      if (!getToken(req.headers)) {
        return res.status(403).send({
          success: false,
          msg: "Unauthorized.",
        });
      }

      //Get Body params
      let body = req.body;

      //Reorder Item
      body.reorderedCluster.forEach(async (x) => {
        await Page.updateOne(
          {
            _id: body.page,
            owner: req.user._id,
            "posts._id": x._id,
          },
          {
            $set: {
              "posts.$.order": x.order,
            },
          }
        );
      });

      //Return OK
      res.json({
        success: "ok",
      });
    }
  );

  router.post(
    "/post/delete",
    passport.authenticate("jwt", {
      session: false,
    }),
    async function(req, res) {
      //Authenticate
      if (!getToken(req.headers)) {
        return res.status(403).send({
          success: false,
          msg: "Unauthorized.",
        });
      }

      //Get Body params
      let body = req.body;

      // console.log(body);

      //Find Page and Save edits
      let post = await Page.updateOne(
        {
          _id: body.page,
          owner: req.user._id,
        },
        {
          $pull: {
            posts: {
              _id: body._id,
            },
          },
        }
      );

      // console.log(post);

      //Return OK
      res.json({
        success: "ok",
      });
    }
  );
  router.post("/post/load", async function(req, res) {});
};
module.exports = api;
