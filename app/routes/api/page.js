var Site = require("../../models/Site.js");
var Page = require("../../models/Page.js");
var Photo = require("../../models/Photo.js");
let _ = require("lodash");
let instagram = require("../../scraper/instagram.js");
var slugify = require("slugify");
slugify.extend({ ".": "-" });
var passport = require("passport");
require("../../config/passport")(passport);
var axios = require("axios");
var uniqid = require("uniqid");

let api = async function(router, getToken) {
  //Pages
  /*
Create Page /page/create
Edit Page /page/edit/:id
Delete Page /page/delete/:id
Update Page Design /page/update/design
Set page as home /page/update/home
Get page with photos /page/load/:id
Get page info only /page/get/info
*/

  router.post(
    "/page/create",
    passport.authenticate("jwt", { session: false }),
    async function(req, res) {
      //Authenticate
      if (!getToken(req.headers)) {
        return res.status(403).send({
          success: false,
          msg: "Unauthorized.",
        });
      }

      //Get Body params
      let body = req.body;

      //Find site and update
      let site = await Site.findOne({
        _id: body.site,
        owner: req.user._id,
      });

      let posts = [];

      if (body.type === "instagram") {
        let photos = await Photo.findOne({
          slug: slugify(site.social.handle),
        });

        // Filter
        let filteredPhotos = [...photos.photos];
        if (body.filter !== "") {
          filteredPhotos = photos.photos.filter((x) => {
            return x.hashtags.includes(body.filter);
          });
        }

        //Map Posts
        posts = filteredPhotos.map((x, i) => {
          return {
            tag: x.id,
            title: "",
            image: x.thumbnail_src,
            caption: x.caption,
            hashtags: x.hashtags,
            textarea: "",
            link: "",
            type: "instagram",
            order: i,
          };
        });
      }
      if (body.type === "link") {
        posts = [
          {
            tag: uniqid(),
            title: body.title,
            image: "",
            caption: "",
            hashtags: "",
            textarea: "",
            link: body.link,
            type: "link",
          },
        ];
      }
      if (body.type === "paragraph") {
        posts = [
          {
            tag: uniqid(),
            title: body.title,
            image: body.image,
            caption: body.caption,
            hashtags: "",
            textarea: body.textarea,
            link: body.link,
            type: "paragraph",
          },
        ];
      }

      console.log(posts);

      //Create Page Model
      let pageType = body.type === "instagram" ? "instagram" : "links";
      let page = new Page({
        name: body.name,
        site: body.site,
        slug: slugify(body.name).toLowerCase(),
        social: {
          platform: "instagram",
          handle: site.social.handle,
          filter: body.filter,
        },
        design: {
          type: "style",
          style: JSON.stringify(body.style),
        },
        posts: [...posts],
        type: pageType,
        owner: req.user._id,
      });
      //Save Model
      await page.save();

      //Add Page to Site Navigation
      // let site = await Site.findOne({ _id: body.site });
      site.navigation.push(page._id);
      await site.save();

      //Return OK
      res.json({
        success: "ok",
      });
      //Scrape Instagram Handle for Page
      // await instagram.scrape(req.body.handle);
    }
  );
  router.post(
    "/page/edit",
    passport.authenticate("jwt", { session: false }),
    async function(req, res) {
      //Authenticate
      if (!getToken(req.headers)) {
        return res.status(403).send({
          success: false,
          msg: "Unauthorized.",
        });
      }

      //Get Body params
      let body = req.body;

      //Find Page and Save edits
      let page = await Page.findOne({
        _id: body._id,
        owner: req.user._id,
      });

      page.name = body.name;
      page.slug = body.slug;
      // page.social.handle = body.handle;
      // page.social.filter = body.filter;
      await page.save();

      //Return OK
      res.json({
        success: "ok",
      });
    }
  );
  router.post(
    "/page/delete",
    passport.authenticate("jwt", { session: false }),
    async function(req, res) {
      //Authenticate
      if (!getToken(req.headers)) {
        return res.status(403).send({
          success: false,
          msg: "Unauthorized.",
        });
      }

      //Get Body params
      let body = req.body;

      //Get Page and Edit
      let page = await Page.findOne({
        _id: body._id,
        owner: req.user._id,
      });

      //Get Site
      let site = await Site.findOne({
        _id: page.site,
        owner: req.user._id,
      });
      //Remove from navigation and save
      site.navigation = site.navigation.filter((x) => x != page._id);
      await site.save();

      //Remove Page
      await page.remove();

      //Return
      res.json({
        success: "ok",
      });
    }
  );
  router.post(
    "/page/update/design",
    passport.authenticate("jwt", { session: false }),
    async function(req, res) {
      //Authenticate
      if (!getToken(req.headers)) {
        return res.status(403).send({
          success: false,
          msg: "Unauthorized.",
        });
      }

      //Get Body params
      let body = req.body;

      //Get Page and Edit
      let page = await Page.findOne({
        _id: body._id,
        owner: req.user._id,
      });
      page.design.style = JSON.stringify(body.style);
      await page.save();

      //Return
      res.json({ success: "ok" });
    }
  );
  router.post(
    "/page/update/home",
    passport.authenticate("jwt", { session: false }),
    async function(req, res) {
      //Authenticate
      if (!getToken(req.headers)) {
        return res.status(403).send({
          success: false,
          msg: "Unauthorized.",
        });
      }

      //Get Body params
      let body = req.body;

      //Get Site Information and edit home
      let site = await Site.findOne({
        _id: body.site,
        owner: req.user._id,
      });
      site.home = body.page;
      await site.save();

      //Return
      res.json({ success: "ok" });
    }
  );
  router.get("/page/get/info/:id", async function(req, res) {});
  router.post("/page/load", async function(req, res) {
    //Get params
    let body = req.body;

    //Get Site
    let site = await Site.findOne({
      slug: body.site,
    });
    //Get Page
    let page = await Page.findOne({
      slug: body.slug,
      site: site._id,
    });

    console.log(page.posts);

    //Return
    res.json({
      page: page,
      posts: page.posts.sort((a, b) => b.order - a.order, 0),
      site: site,
      design: site.design,
    });
  });
};
module.exports = api;
