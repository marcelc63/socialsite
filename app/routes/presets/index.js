var passport = require("passport");
var settings = require("../../config/settings");
require("../../config/passport")(passport);
var express = require("express");
var jwt = require("jsonwebtoken");

let api = async function(router, getToken) {
  router.get("/presets", function(req, res) {
    let preset = require("./basic.json");
    res.json(preset);
  });
};

module.exports = api;
