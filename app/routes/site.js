var express = require("express");
var router = express.Router();
var path = require("path");

var Site = require("../models/Site.js");
var Page = require("../models/Page.js");
var Photo = require("../models/Photo.js");

var defaultStyle = require("../../src/schema/defaultStyle.js").default;

var slugify = require("slugify");
slugify.extend({ ".": "-" });

let basicPaths = [
  "/",
  "/login",
  "/register",
  "/dashboard",
  "/dashboard/:site",
  "/dashboard/:site/:page",
  "/editor/:site",
  "/editor/:site/:page",
];

function parseStyle(payload) {
  // console.log(defaultStyle);
  return defaultStyle.map((x) => {
    let component = payload.filter((s) => s.component === x.component);

    return {
      ...x,
      configs: x.configs.map((s) => {
        if (component.length !== 0 && s.input !== "none") {
          let config = component[0].configs.filter(
            (t) => t.property === s.property
          );
          if (config.length !== 0) {
            s.value = config[0].value;
          }
        }
        return {
          ...s,
        };
      }),
    };
  });
}

router.get(basicPaths, function(req, res) {
  res.sendFile(path.join(__dirname + "/../../dist/index.html"));
});

async function generateSite(req, res, type) {
  //Get Site
  let site = await Site.findOne({
    slug: req.params.site,
  });
  let isMobile = /mobile/i.test(req.headers["user-agent"]);

  //Get Page
  let pageConfig =
    type === "site"
      ? {
          _id: site.home,
        }
      : {
          slug: req.params.page,
          site: site._id,
        };
  let page = await Page.findOne(pageConfig);
  //Get Pages
  let pages = await Page.find({
    site: site._id,
  });

  function convertToClass(component, componentStyle) {
    let thisClass = componentStyle.filter((x) => x.component === component)[0];

    let configs = isMobile
      ? thisClass.configs.concat(thisClass.mobile)
      : thisClass.configs.concat(thisClass.desktop);

    let getClass = configs.filter((x) => x.type === "class" && !x.editorOnly);
    let parsedClass = getClass.map((x) => x.value);
    let string = component + " " + parsedClass.join(" ");
    return string;
  }
  function convertToStyle(component, componentStyle) {
    let thisStyle = componentStyle.filter((x) => x.component === component)[0];

    let configs = isMobile
      ? thisStyle.configs.concat(thisStyle.mobile)
      : thisStyle.configs.concat(thisStyle.desktop);

    if (component === "background") {
      console.log(configs);
    }

    let getStyle = configs.filter((x) => x.type === "style" && !x.editorOnly);
    let parsedStyle = getStyle.map((x) => x.property + ":" + x.value);
    let string = parsedStyle.join(";") + ";";
    return string;
  }

  function getStyle(component, componentStyle) {
    return {
      class: convertToClass(component, componentStyle),
      style: convertToStyle(component, componentStyle),
    };
  }

  let siteStyle = parseStyle(JSON.parse(site.design.style));

  let navigation = [];
  if (pages.length > 0) {
    navigation = site.navigation.map((x) => {
      let result = pages.filter((s) => s.id === x)[0];
      return result;
    });
  }

  //Return
  let result = {
    page: { name: page.name },
    posts: page.posts.reverse(),
    site: {
      name: site.name,
      slug: site.slug,
    },
    navigation: navigation.map((x) => {
      return {
        name: x.name,
        slug: x.slug,
        url: site.home == x.id ? `/${site.slug}` : `/${site.slug}/${x.slug}`,
      };
    }),
    design: {
      background: getStyle("background", siteStyle),
      body: getStyle("body", siteStyle),
      card: getStyle("card", siteStyle),
      cardImage: getStyle("card__img", siteStyle),
      cardCaption: getStyle("card__caption", siteStyle),
      cardSpacing: getStyle("card__spacing", siteStyle),
      header: getStyle("header", siteStyle),
      headerTitle: getStyle("header__title", siteStyle),
      navigation: getStyle("navigation", siteStyle),
      navigationLink: getStyle("navigation__link", siteStyle),
      link: getStyle("link", siteStyle),
      paragraph: getStyle("paragraph", siteStyle),
      paragraphContent: getStyle("paragraph__content", siteStyle),
      paragraphDescription: getStyle("paragraph__description", siteStyle),
      paragraphTitle: getStyle("paragraph__title", siteStyle),
    },
  };

  console.log(page.type);
  await res.render("links", {
    ...result,
  });
  // if (page.type === "instagram") {
  //   await res.render("site", {
  //     ...result,
  //   });
  // }
  // if (page.type === "links") {
  //   await res.render("links", {
  //     ...result,
  //   });
  // }
}

router.get("/:site", async function(req, res) {
  generateSite(req, res, "site");
});

router.get("/:site/:page", async function(req, res) {
  generateSite(req, res, "page");
});

module.exports = router;
