var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var ObjectId = mongoose.Schema.Types.ObjectId;
var URLSlugs = require("mongoose-url-slugs");

var Navigation = new Schema({
  type: String,
  destination: String,
  page: ObjectId,
});

var Design = new Schema({
  type: String,
  style: String,
});

var Social = new Schema({
  platform: String,
  handle: String,
  // filter: String,
});

var MySchema = new Schema({
  name: String,
  description: String,
  logo: String,
  navigation: [String],
  home: ObjectId,
  social: [Social],
  design: Design,
  hashtags: Array,
  owner: { type: ObjectId, ref: "User" },
});

MySchema.plugin(URLSlugs("name", { update: true }));

module.exports = mongoose.model("Site", MySchema);
