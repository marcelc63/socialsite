var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var ObjectId = mongoose.Schema.Types.ObjectId;
var URLSlugs = require("mongoose-url-slugs");

var Photos = new Schema({
  classification: String,
  id: String,
  display_url: String,
  shortcode: String,
  dimensions: {
    height: String,
    width: String
  },
  thumbnail_src: String,
  thumbnail_resources: [
    {
      src: String,
      config_width: String,
      config_height: String
    }
  ],
  caption: String,
  likes: String,
  comments: String,
  hashtags: [String],
  raw: String
});

var MySchema = new Schema({
  profile: {
    biography: String,
    fullname: String,
    username: String,
    profile_pic_url_hd: String,
    profile_pic_url: String,
    followers: String,
    following: String,
    posts: String,
    external_url: String,
    hashtags: [String],
    raw: String
  },
  photos: [Photos]
});

MySchema.plugin(URLSlugs("profile.username", { update: true }));

module.exports = mongoose.model("Photos", MySchema);
