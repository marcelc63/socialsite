var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var bcrypt = require("bcryptjs");
var SALT_WORK_FACTOR = 10;

var MySchema = new Schema({
  password: {
    type: String,
    required: true
  },
  email: {
    type: String,
    unique: true,
    required: true
  },
  usergroup: {
    type: String
  },
  datestamp: {
    type: Date,
    // `Date.now()` returns the current unix timestamp as a number
    default: Date.now
  }
});

MySchema.pre("save", function(next) {
  var user = this;
  console.log(this);
  if (this.isModified("password") || this.isNew) {
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
      if (err) {
        return next(err);
      }
      console.log("salt", salt);
      bcrypt.hash(user.password, salt, function(err, hash) {
        if (err) {
          return next(err);
        }
        console.log(hash);
        user.password = hash;
        console.log(user.password);
        next();
      });
    });
  } else {
    return next();
  }
});

MySchema.methods.comparePassword = function(password, cb) {
  console.log("compare", password, this);
  bcrypt.compare(password, this.password, function(err, isMatch) {
    if (err) {
      return cb(err);
    }
    cb(null, isMatch);
  });
};

module.exports = mongoose.model("User", MySchema);
