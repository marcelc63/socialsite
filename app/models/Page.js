var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var ObjectId = mongoose.Schema.Types.ObjectId;
var URLSlugs = require("mongoose-url-slugs");

var Social = new Schema({
  platform: String,
  handle: String,
  filter: String,
});

var Design = new Schema({
  type: String,
  style: String,
});

var Post = new Schema({
  tag: String,
  title: String,
  image: String,
  caption: String,
  hashtags: Array,
  textarea: String,
  link: String,
  type: String,
  order: { type: Number, default: 0 },
});

var MySchema = new Schema({
  name: String,
  slug: String,
  site: ObjectId,
  social: Social,
  design: Design,
  posts: [Post],
  type: String,
  owner: { type: ObjectId, ref: "User" },
});

// MySchema.plugin(URLSlugs("name", { update: true }));

module.exports = mongoose.model("Page", MySchema);
