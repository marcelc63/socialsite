var express = require("express");
var Photo = require("../models/Photo.js");
let _ = require("lodash");
let axios = require("axios");

async function scrape(username) {
  let count = await Photo.countDocuments({ "profile.username": username });
  console.log(count > 0);
  //docs.length > 0
  if (count > 0) {
    let photos = await Photo.findOne({ "profile.username": username });
    console.log("exist");
    return {
      success: true,
      msg: "Photos loaded",
      data: {
        profile: photos.profile,
        photos: photos.photos,
      },
    };
  }

  //if not scrape
  const convert = require("html-to-json-data");
  const {
    group,
    text,
    number,
    href,
    src,
    uniq,
  } = require("html-to-json-data/definitions");

  console.log("starting");

  let response = await axios.get(`https://www.picuki.com/profile/${username}`);
  const json = convert(response.data, {
    data: group(".box-photos li", {
      thumbnail_src: src(".post-image", ""),
      caption: text(".photo-description"),
    }),
  });

  console.log(json);

  let allHashtags = [];

  let photos = json.data
    .filter((x) => x.thumbnail_src !== "")
    .map((photo) => {
      let hashtags = photo.caption
        .split(" ")
        .map((x) => {
          if (x.substr(0, 1) === "#") {
            x = x.replace("#", "");
            x = x.replace(",", "");
            x = x.toLowerCase();
            return x;
          }
          return false;
        })
        .filter((x) => x);
      allHashtags = allHashtags.concat(hashtags);
      return {
        ...photo,
        hashtags: [...hashtags],
      };
    })
    .reverse();

  let profile = {
    username,
    hashtags: [..._.uniq(allHashtags)],
  };

  let model = new Photo({
    profile: {
      ...profile,
    },
    photos: [...photos],
  });
  let newModel = await model.save();

  return {
    success: true,
    msg: "Photos Saved",
    data: {
      profile,
      photos,
    },
  };
}

module.exports = {
  scrape: scrape,
};
