let axios = require("axios");
const fs = require("fs");
let INSTAGRAM_URL = "https://www.instagram.com";
let getFirstPage = require("./first_page.js");
let getNextPage = require("./next_page.js");

async function getUser(user, limit = 1000, type = "live") {
  console.log(user);
  try {
    let config = {
      headers: {
        "User-Agent":
          "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0",
        Cookie:
          'mid=XBDcEgAEAAHA28QSyDt4fJcYa-Q4; mcd=3; datr=K0hBXGZkTCidTJfoxos1fRFM; csrftoken=w3wxL60jAKzp9MEhknb7wyG2iShZqrbf; ds_user_id=338353675; sessionid=338353675%3Aj8bOKK0RPrMJpt%3A15; ig_did=FD2F66D6-CF5D-413F-8E39-D5ADC246C1B7; shbid=13264; shbts=1592720142.7491584; rur=PRN; urlgen="{"23.249.172.244": 9009\054 "175.158.55.231": 135478}:1jmtH3:sZt_kl_nCu-j0z62_Gy-E439E3A"',
        "x-instagram-gis": "2a49b08c8be3053ebe6e178836fe0ad7",
        "x-requested-with": "XMLHttpRequest",
      },
    };
    const response = await axios.get(`${INSTAGRAM_URL}/${user}`, {}, config);

    let body = response.data;

    if (body) {
      var data = body.match("window._sharedData = ([^]*)};</script>");
      data = data
        ? JSON.parse(
            body.match("window._sharedData = ([^]*)};</script>")[1] + "}"
          )
        : false;
      // return false;
      let queryID = await getFirstPage(body);
      //   Define
      let profilePage = data.entry_data.ProfilePage[0];
      let payload = {
        profile_id: profilePage.graphql.user.id,
        csrf: data.config.csrf_token,
        post_per_req: 50,
        end_cursor: "",
        rhx_gis: "",
        query_id: queryID,
        totalCount: 0,
      };
      let scraped = await getNextPage(payload, limit);
      // console.log(profilePage);
      console.log(user);
      console.log(queryID);
      // console.log(scraped);
      console.log(scraped.photos.length);
      let toSave = {
        profile: {
          biography: profilePage.graphql.user.biography,
          full_name: profilePage.graphql.user.full_name,
          username: profilePage.graphql.user.username,
          profile_pic_url: profilePage.graphql.user.profile_pic_url,
          profile_pic_url_hd: profilePage.graphql.user.profile_pic_url_hd,
          followers: profilePage.graphql.user.edge_followed_by.count,
          following: profilePage.graphql.user.edge_follow.count,
          posts: profilePage.graphql.user.edge_owner_to_timeline_media.count,
          external_url: profilePage.graphql.user.external_url,
          private: false,
          raw: type === "test" ? profilePage : "",
        },
        photos: scraped.photos.map((x) => {
          let res = x.node;
          return {
            classification: res.__typename,
            id: res.id,
            display_url: res.display_url,
            shortcode: res.shortcode,
            dimensions: res.dimensions,
            thumbnail_src: res.thumbnail_src,
            thumbnail_resources: res.thumbnail_resources.reverse(),
            caption:
              res.edge_media_to_caption.edges.length !== 0
                ? res.edge_media_to_caption.edges[0].node.text
                : "",
            likes: res.edge_media_preview_like.count,
            comments: res.edge_media_to_comment.count,
            raw: type === "test" ? res : JSON.stringify(res),
          };
        }),
      };

      toSave = {
        ...toSave,
        stats: {
          totalLikes: toSave.photos.reduce((a, b) => {
            return a + parseInt(b.likes);
          }, 0),
          totalComments: toSave.photos.reduce(
            (a, b) => parseInt(a) + parseInt(b.comments),
            0
          ),
        },
      };

      if (toSave.photos.length === 0) {
        toSave.profile.private = true;
      }
      // if (type === "live") {
      //   delete toSave.profile.raw;
      //   delete toSave.photos.raw;
      // }

      // fs.writeFile("./test.json", JSON.stringify(toSave), function(err) {
      //   if (err) {
      //     return console.log(err);
      //   }
      //   console.log("The file was saved!");
      // });

      // let saveRaw = {
      //   profilePage,
      //   scraped
      // };
      // fs.writeFile("./raw.json", JSON.stringify(saveRaw), function(err) {
      //   if (err) {
      //     return console.log(err);
      //   }

      //   console.log("The file was saved!");
      // });
      return toSave;
    }
  } catch (error) {
    console.error(error);
  }
}

async function getUserBasic(user) {
  let config = {
    headers: {
      "User-Agent":
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0",
    },
  };
  const response = await axios.get(
    `${INSTAGRAM_URL}/${user}?__a=1`,
    {},
    config
  );

  console.log(response.data);

  let graphql = response.data.graphql;
  let photos = graphql.user.edge_owner_to_timeline_media.edges;
  let toSave = {
    profile: {
      biography: graphql.user.biography,
      full_name: graphql.user.full_name,
      username: graphql.user.username,
      profile_pic_url: graphql.user.profile_pic_url,
      profile_pic_url_hd: graphql.user.profile_pic_url_hd,
      followers: graphql.user.edge_followed_by.count,
      following: graphql.user.edge_follow.count,
      posts: graphql.user.edge_owner_to_timeline_media.count,
      external_url: graphql.user.external_url,
      private: false,
    },
    photos: photos.map((x) => {
      let res = x.node;
      return {
        classification: res.__typename,
        id: res.id,
        display_url: res.display_url,
        shortcode: res.shortcode,
        dimensions: res.dimensions,
        thumbnail_src: res.thumbnail_src,
        thumbnail_resources: res.thumbnail_resources.reverse(),
        caption:
          res.edge_media_to_caption.edges.length !== 0
            ? res.edge_media_to_caption.edges[0].node.text
            : "",
        likes: res.edge_media_preview_like.count,
        comments: res.edge_media_to_comment.count,
      };
    }),
  };
  return toSave;
}

async function getUserAlternative(user) {
  const convert = require("html-to-json-data");
  const {
    group,
    text,
    number,
    href,
    src,
    uniq,
  } = require("html-to-json-data/definitions");

  let response = await axios.get("https://www.picuki.com/profile/jauwactive");
  const json = convert(response.data, {
    page: "GitHub",
    repos: group(".box-photos li", {
      image: src(".post-image", ""),
      caption: text(".photo-description"),
    }),
  });

  let result = json.filter((x) => x.image !== "");
  return result;
}

module.exports = getUserBasic;
