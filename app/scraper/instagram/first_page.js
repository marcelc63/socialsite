let axios = require("axios");
var md5 = require("md5-hex");
let INSTAGRAM_URL = "https://www.instagram.com";

async function getFirstPage(data) {
  var profileURL = data.match("/static(.*)ProfilePageContainer.js/(.*).js")[0];
  try {
    let config = {
      headers: {
        "User-Agent":
          "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Safari/537.36",
        Cookie:
          'mid=XBDcEgAEAAHA28QSyDt4fJcYa-Q4; mcd=3; datr=K0hBXGZkTCidTJfoxos1fRFM; csrftoken=w3wxL60jAKzp9MEhknb7wyG2iShZqrbf; ds_user_id=338353675; sessionid=338353675%3Aj8bOKK0RPrMJpt%3A15; ig_did=FD2F66D6-CF5D-413F-8E39-D5ADC246C1B7; shbid=13264; shbts=1592720142.7491584; rur=PRN; urlgen="{"23.249.172.244": 9009\054 "175.158.55.231": 135478}:1jmtH3:sZt_kl_nCu-j0z62_Gy-E439E3A"',
      },
    };
    const response = await axios.get(
      `${INSTAGRAM_URL}${profileURL}`,
      {},
      config
    );
    let body = response.data;
    if (body) {
      let queryID = body.match(
        'profilePosts.byUserId.get([^"]*)queryId:"([^"]*)"'
      )[2];
      return queryID;
    }
  } catch (error) {
    console.error(error);
  }
}

module.exports = getFirstPage;
